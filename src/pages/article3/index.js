import React from 'react'
import Layout from "../../components/Layout"
import './article3.css'
import SingleAd from '../../components/SingleAd/SingleAd'
import SinglePostData from '../../components/SinglePostData/SinglePostData'
import slideshow from '../../img/slideshow.png'

const Article3Page = () => (

    <Layout>
        <div className="container">
        <div className='column is-12 Article3Page' style={{padding: '0px'}}>
        <div className='is-3 Article3Page_col3'>
            <SingleAd />
            <SingleAd />
        </div>
        <div className='is-6 Article3Page_data'>
            <SinglePostData />
        </div>
        <div className='is-3 Article3Page_col3'>
        <SingleAd />
        <SingleAd />
        </div>
        </div>
        <div className='Article3Page_ad'><img src={slideshow} /></div>
        </div>
    </Layout>
) 

export default  Article3Page