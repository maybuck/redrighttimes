import React from 'react'
import { navigate } from 'gatsby-link'
import Layout from '../../components/Layout'
import SingleAd from '../../components/SingleAd/SingleAd'
import PrivacyPolicy from '../../components/PrivacyPolicy/PrivacyPolicy'
import NewsLetterBlock from '../../components/NewsLetterBlock/NewsLetterBlock'
import JustTitlePostDate from '../../components/JustTitlePostDate/JustTitlePostDate'
import twitter_ad from '../../img/twitter_ad.png'
import './privacy_policy.css'




function encode(data) {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

export default class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    const form = e.target
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        ...this.state,
      }),
    })
      .then(() => navigate(form.getAttribute('action')))
      .catch((error) => alert(error))
  }

  render() {
    return (
      <Layout>
        <div className='container'>
      <SingleAd />
      
      <div className='privacy_policy_right'>
      <div className='column is-8'>
      <PrivacyPolicy />
      <PrivacyPolicy />
      <PrivacyPolicy />
      <PrivacyPolicy />
      <PrivacyPolicy />
      <PrivacyPolicy />
      </div>
      <div className='column is-4 privacy_policy_sidebar'>
        <div className='Breaking_New'>
    <h2>Breaking News</h2>
    <img src={twitter_ad} style={{width:'100%', 'margin': '20px 0px 0px 0px'}}/>
    <h1>West Java to review Meykardah project amid alleged bribery case</h1> 
    </div>
    <div className='privacy_policy_newsletter'><NewsLetterBlock /></div>
    <div className='right_side_bannerAd'><SingleAd /></div>
    <div className='Top_News'>
      <h2>TopNews</h2>
      </div>
      <JustTitlePostDate />
      <JustTitlePostDate />
      <JustTitlePostDate />
      <JustTitlePostDate />
      <div className='right_side_bannerAd'><SingleAd /></div>
      <div className='trending_truth_row_ad_height'><SingleAd /></div>
      </div>
      </div>
        </div>
      </Layout>
    )
  }
}
