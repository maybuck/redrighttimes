import React from "react";
import { Link } from "gatsby"
import './nav.css'


function navbar(){

    return(
        <div>

        <nav className="column is-12">
        <ul>
        <li><Link to="/">HomePage</Link></li>
            <li><Link to="">Trending Truths</Link></li>
            <li><Link to="/about">About Us</Link></li>
            <li><Link to="/contact">Privacy Policy</Link></li>
            <li><Link to="/#">Contact Us</Link></li>
        </ul>
        </nav>
        </div>
    )
}

export default navbar